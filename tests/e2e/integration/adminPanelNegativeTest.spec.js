let data

beforeEach(() => {
  cy.fixture('admin').then(fixturedata => {
     data = fixturedata
  })
})

describe('Admin Panel Negative Test', () => {
  beforeEach(() => {
      cy.visit(data.url);
  })
  it('Non exist login test', () => {
      cy.log('Enter password && login')
      cy.get(':nth-child(2) > .el-form-item__content > .el-input > .el-input__inner').type(data.fakelogin);
      cy.get(':nth-child(3) > .el-form-item__content > .el-input > .el-input__inner').type(data.password);
      cy.log('Click on login button')
      cy.get('.el-button--primary').click()
      cy.get('.el-message__content').should("have.text", "Пользователь не найден");
  });

  it('Non exist password test', () => {
      cy.log('Enter password && login')
      cy.get(':nth-child(2) > .el-form-item__content > .el-input > .el-input__inner').type(data.login);
      cy.get(':nth-child(3) > .el-form-item__content > .el-input > .el-input__inner').type(data.fakepassword);
      cy.log('Click on login button')
      cy.get('.el-button--primary').click()
      cy.get('.el-message__content').should("have.text", "Пароль не верный");
  });

  it('password & login length', () => {
      cy.log('Enter password && login')
      cy.get(':nth-child(2) > .el-form-item__content > .el-input > .el-input__inner').type('ad');
      cy.get(':nth-child(3) > .el-form-item__content > .el-input > .el-input__inner').type('12');
      cy.log('Click on login button')
      cy.get('.el-button--primary').click()
      cy.get(':nth-child(2) > .el-form-item__content > .el-form-item__error')
        .should((el) => {
          expect(el.text().trim()).equal('Длинна минимум 3 символа');
        });
      cy.get(':nth-child(2) > .el-form-item__content > .el-form-item__error')
        .should((el) => {
          expect(el.text().trim()).equal('Длинна минимум 3 символа');
        });
  });

  it('empty & login length', () => {
      cy.log('Go to admin panel')
      cy.visit(data.url);
      cy.log('Enter password && login')
      cy.log('Click on login button')
      cy.get('.el-button--primary').click()
      cy.get(':nth-child(2) > .el-form-item__content > .el-form-item__error')
        .should((el) => {
          expect(el.text().trim()).equal('Логин не должо быть пустым');
        });
      cy.get(':nth-child(3) > .el-form-item__content > .el-form-item__error')
        .should((el) => {
          expect(el.text().trim()).equal('Введите пароль');
        });
  });

});
