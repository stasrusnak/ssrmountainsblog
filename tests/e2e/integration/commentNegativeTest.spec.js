const faker = require("faker");
describe('Comment test', () => {
  beforeEach(() => {
    cy.visit('/');
  })

  it('name length', () => {
    cy.get(':nth-child(1) > .el-card__body > .post-footer > .el-button').click()
    cy.wait(200)
    cy.get('.el-form-item__content > .el-input > .el-input__inner').type('Fa');
    cy.get('.el-textarea__inner').type(faker.lorem.sentence());
    cy.get('.el-form-item__content > .el-button').click()
    cy.get('.el-form-item__error')
      .should((el) => {
        expect(el.text().trim()).equal('Длинна минимум 3 символа');
      });
  });

  it('empty Name and comment', () => {
    cy.get(':nth-child(1) > .el-card__body > .post-footer > .el-button').click()
    cy.wait(200)
    cy.get('.el-form-item__content > .el-button').click()
    cy.get(':nth-child(2) > .el-form-item__content > .el-form-item__error')
      .should((el) => {
        expect(el.text().trim()).equal('Имя не должо быть пустым');
      });
    cy.get(':nth-child(3) > .el-form-item__content > .el-form-item__error')
      .should((el) => {
        expect(el.text().trim()).equal('Введите ваш комментарий');
      });
  });

});
