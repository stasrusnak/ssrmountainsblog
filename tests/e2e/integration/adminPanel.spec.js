let data
const faker = require("faker");
beforeEach(() => {
  cy.fixture('admin').then(fixturedata => {
    data = fixturedata
  })
})

describe('Admin page', () => {

  beforeEach(() => {
    cy.visit(data.url);
    cy.log('Enter password && login')
    cy.get(':nth-child(2) > .el-form-item__content > .el-input > .el-input__inner').type(data.login);
    cy.get(':nth-child(3) > .el-form-item__content > .el-input > .el-input__inner').type(data.password);
    cy.log('Click on login button')
    cy.get('.el-button--primary').click()
  })

  it('write fake comment', () => {
    cy.visit('/');
    cy.get(':nth-child(1) > .el-card__body > .post-footer > .el-button').click()
    cy.get('.el-form-item__content > .el-input > .el-input__inner').type('Fake name:' + faker.name.findName());
    cy.get('.el-textarea__inner').type(faker.lorem.sentence());
    cy.get('.el-form-item__content > .el-button').click()
    cy.get('.el-message')
      .should((el) => {
        expect(el.text().trim()).equal('Комментарий добавлен');
      });
  });

  it('Remove fake comments', () => {
    cy.get('.el-menu > :nth-child(4)').click()
    cy.get('.desktop > .el-container > .el-main > .commentScreen > .el-input > .el-input__inner').type('Fake');
    cy.get('.desktop > .el-container > .el-main > .commentScreen > .el-tree > :nth-child(1) > [style="padding-left: 0px;"] > .el-checkbox > .el-checkbox__input > .el-checkbox__inner').click()
    cy.get('.desktop > .el-container > .el-main > .commentScreen > :nth-child(5) > .el-button--warning').click()
    cy.get('.el-button.el-button--primary.el-button--mini').click({ multiple: true,force: true })
    cy.get('.el-notification').should((el) => {
      expect(el.text().trim()).equal('Комментарий удален');
    });
  });


});
