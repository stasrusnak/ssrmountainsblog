const faker = require("faker")
describe('Admin Panel Negative Test ', () => {
  beforeEach(() => {
    cy.visit('/');
  })
  it('Test resizable page', () => {
    cy.viewport('macbook-15')
    cy.wait(200)
    cy.viewport('macbook-13')
    cy.wait(200)
    cy.viewport('macbook-11')
    cy.wait(200)
    cy.viewport('ipad-2')
    cy.wait(200)
    cy.viewport('ipad-mini')
    cy.wait(200)
    cy.viewport('iphone-6+')
    cy.wait(200)
    cy.viewport('iphone-6')
    cy.wait(200)
    cy.viewport('iphone-5')
    cy.wait(200)
    cy.viewport('iphone-4')
    cy.wait(200)
    cy.viewport('iphone-3')
    cy.wait(200)
    cy.viewport('ipad-2', 'portrait')
    cy.wait(200)
    cy.viewport('iphone-4', 'landscape')
    cy.wait(200)
  });

  it('Open post & go to back', () => {
    cy.get(':nth-child(1) > .el-card__body > .post-footer > .el-button').click()
    cy.get('.nuxt-link-active > .el-button').click()
  });

  it('Open post & write comment', () => {
    cy.get(':nth-child(1) > .el-card__body > .post-footer > .el-button').click()
    cy.get('.el-form-item__content > .el-input > .el-input__inner').type('Fake name:' + faker.name.findName());
    cy.get('.el-textarea__inner').type(faker.lorem.sentence());
    cy.get('.el-form-item__content > .el-button').click()
    cy.get('.el-message')
      .should((el) => {
        expect(el.text().trim()).equal('Комментарий добавлен');
      });
  });

});
