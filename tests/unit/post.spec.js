import {shallowMount, config, createLocalVue} from "@vue/test-utils";
import {FakePost} from '~/tests/unit/fixtures/postFaker';
import ElementUI from 'element-ui';
import post from "~/components/main/Post";
import VueRouter from 'vue-router'

const localVue = createLocalVue();
localVue.use(ElementUI);
localVue.use(VueRouter)
const router = new VueRouter()
config.showDeprecationWarnings = false

let wrapper
beforeEach(() => {
  wrapper = shallowMount(post, {
    localVue,
    router,
    propsData: {
      post: FakePost
    }
  })
})

describe("Component post", () => {
  test("Component post should match snapshot", () => {
    const wrapper = shallowMount(post, {
      localVue,
      propsData: {
        post: {
          _id: 'testuuid',
          views: 888,
          comments: {},
          title: 'test title',
          text: 'test text',
          imageURL: 'http://placeimg.com/640/480/nature',
          date: new Date('01-01-1970').toLocaleString()
        },
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy();
    expect(wrapper.html()).toMatchSnapshot()
  });

  test("Test with Fake date Post", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
    expect(wrapper.is(post)).toBeTruthy();
    expect(wrapper.vm.post).toEqual(FakePost)
  });

  test("Open Post", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
    expect(wrapper.vm.openPost())
  });

});



