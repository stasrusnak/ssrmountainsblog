import { shallowMount, config, createLocalVue } from "@vue/test-utils";
import ElementUI from 'element-ui';
import index from "~/pages/index";
const localVue = createLocalVue();

localVue.use(ElementUI);
config.showDeprecationWarnings = false

describe("index", () => {
  test("mounts index page", () => {
    const wrapper = shallowMount(index,{localVue});
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test("index should match snapshot", () => {
    const wrapper = shallowMount(index,{localVue});
    expect(wrapper.html()).toMatchSnapshot()
  });

});



