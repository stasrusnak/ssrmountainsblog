import {shallowMount, config, createLocalVue} from "@vue/test-utils";
import ElementUI from 'element-ui';
import MobileNavigation from "~/components/main/MobileNavigation";
import VueRouter from 'vue-router'
const localVue = createLocalVue();
localVue.use(ElementUI);
localVue.use(VueRouter)
const router = new VueRouter()
config.showDeprecationWarnings = false

let wrapper
beforeEach(() => {
   wrapper = shallowMount(MobileNavigation, {localVue,router});
})

describe("MobileNavigation", () => {
  test("mounts MobileNavigation page", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test("MobileNavigation should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot()
  });

  test("MobileNavigation click on link", () => {
    wrapper.find('el-dropdown-item-stub').trigger('click')
  });

  test("MobileNavigation test handleCommand methods", () => {
    expect(wrapper.vm.handleCommand('/admin'))
  });
});



