import {shallowMount, config, createLocalVue} from "@vue/test-utils";
import {FakeComment} from '~/tests/unit/fixtures/commentFaker'
import ElementUI from 'element-ui';
import comment from "~/components/main/Comment";

const localVue = createLocalVue();
localVue.use(ElementUI);
config.showDeprecationWarnings = false

describe("Component comment", () => {
  test("Component comment should match snapshot", () => {
    const wrapper = shallowMount(comment, {
      localVue,
      propsData: {
        comment: {
          name: "Иван",
          text: "Очень красивое место!!!!",
          date: new Date('01-01-1970').toLocaleString()
        },
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy();
    expect(wrapper.html()).toMatchSnapshot()
  });

  test("toEqual Test with Fake date", () => {
    const wrapper = shallowMount(comment, {
      localVue,
      propsData: {
        comment: FakeComment
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy();
    expect(wrapper.is(comment)).toBeTruthy();
    expect(wrapper.vm.comment).toEqual(FakeComment)
  });


});



