import faker from 'faker'
import {FakeComment} from './commentFaker'

export const FakePost =
  {
    '_id': faker.random.uuid(),
    'views': faker.random.number(),
    'comments': FakeComment,
    'title': faker.name.title(),
    'text': faker.lorem.sentences(),
    'imageURL': faker.image.nature(),
    'date': faker.date.past(),
  }



