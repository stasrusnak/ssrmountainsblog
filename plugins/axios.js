export default function ({$axios, redirect, store}) {

  $axios.interceptors.request.use(request => {
   if (store.getters['baseAuth/isAuthenticated'] && !request.headers.common['Authorization']) {
      const token = store.getters['baseAuth/token']
      request.headers.common['Authorization'] = `Bearer ${token}`
    }
    return request
  })

  $axios.onError(error => {
    if (error.response.status === 401) {
      redirect('/admin/login?msg=session')
      store.dispatch('baseAuth/logout')
    }

    if (error.response.status === 500) {
      console.log('Server error')
    }
  })
}
