

![Netlify](https://miro.medium.com/max/78/1*A0rVKDO9tEFamc-Gqt7oEA.png) ![Netlify](https://travis-ci.org/laravel/framework.svg) 
![imgur](https://i.imgur.com/0oz3Uwp.png)
## Немного про Mountains Blog
### [Blog](https://mountains-blog.herokuapp.com/ "Blog") 
### [Admin Panel](https://mountains-blog.herokuapp.com/admin "Admin Panel]") (Логин / пароль)*только роботодателю

Mountains Blog SSR - это адаптивный веб блог посвященный самым красивым вершинам мира построенный на Nuxt.js с использованием всех его преимуществ и современных компонентов. 

----------
#### Функционал блога позволяет
- Добавлять/ удалять посты и комментарии
- Редактировать посты
- Просматривать статистику просмотров и комментариев в их разрезе
- Осуществлять поиск по всем постам
- Применять ночной / дневной режим  глобально
- Адаптавно просматривать контент на разных устройствах
- Устанавливать приложение как PWA
- И прочее... 
---------
#### Структура проекта
Backend - Express.js

Frontend - Nuxt.js + Element-ui

DataBase - mongodb

Server - heroku

Testing - Jest.js + Cypress.js

Styles - SCSS

Полная функциональность CRUD (создание, чтение, обновление, удаление). 
#### Также перечень используемых библиотек  
- **passport-jwt** авторизация,
- **chart.js** графики, 
- **mongoose** работа с бд, 
- **multer** загрузка фалов, 
- **@nuxtjs/pwa** для создания PWA, 
- **js-cookie** работа с куками, 
- **vue-markdown** для отображения markdown  
- **@nuxtjs/color-mode** для создания ночной темы  
- **faker** созидание фейковых данных.


![enter image description here](https://i.imgur.com/znNwJCl.png)

## Intro
Это приложение использует `SSR + PWA`, обмен данным по средствам `REST API` и реализовано при помощи `vue-cli`  , `vuex store`

----------
### Unit Testing
Запустите команду, которая автоматически найдет все тесты и snapshots

```npm test:unit```

Для обновления snapshots

```npm run test:unit -- -u```

Успешнейший результат прохождения теста

![enter image description here](https://i.imgur.com/AcKAj7d.png)

----------
### E2E cypress testing
Запустите команду, которая автоматически найдет все тесты и запустит их (в консоли без открытия браузера)

```npm test:e2e```

В результате мы должны получить

![enter image description here](https://i.imgur.com/2vxoq2j.png)

## Обзор структуры папок

- `api` -  Эта папка содержит Express.js и всю логику, необходимую для доступа к API
	- `controllers` - контроллеры обрабатывают всю логику за проверкой параметров запроса,отправки ответов с правильными кодами.
	- `middleware` - функции промежуточной обработки для проверки токина и загрузки изображений
	- `models`  - определение схемы модели
	- `routes` - api маршрутизирует карты контроллерам
	- `server` - инициализация среды Express, выбор ключей, подключение к mongodb 	
- `components` - каталог компонентов
	- `chart` - эта папка содержит графики
	- `main` - основные компоненты nuxt
- `layouts` - макеты админ панели, ошибок, дефолтный и прочие для nuxt 
- `middleware` - промежуточной обработчик для проверки токина на frontend уровне nuxt 
- `pages` - это папка содержит все страницы vue 
- `plugins` - Каталог плагинов(авторизация и подключение VueMarkdown) , которые запускаются перед созданием экземпляра корневого приложения Vue.js.
- `static` - содержит статические файлы приложения(фавикон, картинки, прочее..)
- `store` - Vuex store для приложения
- `tests` - эта папка хранит тесты для приложения
  - `e2e` - содержит end-to-end cypress тесты
    - `fixtures ` - JSON файл нужных объектов для тестов 
    - `integration`  - все наши тесты
    - `plugins`  - настраиваемые плагина для запуска на сервере перед тестами
    - `support` - настраиваемые вспомогательные команды
  - `unit` - содержит все unit тесты
    - `__snapshots__` - снепшоты тест кейсов 
    - `fixtures`  - тестовые данные
 - `theme` -  эта папка содержит глобальные css стили приложения
##### Файлы конфигурации 
 - `.editorconfig` - это конфигурационный файл и набор расширений, к большому количеству редакторов кода 
 - `.eslintignore` - отключение правила eslint для конкретного файла
 - `.eslintrc.js` - настройка eslint
 - `babel.config.js` - настройка Babel транспилятора
 - `cypress.json` - этот файл используется для хранения идентификаторов  для использования в тестах
 - `jest.config.js` - задает настройки Jest тестов  
 - `nuxt.config.js` - изменение конфигурации "по умолчанию" в Nuxt.js приложении для обработки отдельных случаев 
 -----
### Установка проекта
Clone the app:

``` git clone https://stasrusnak@bitbucket.org/stasrusnak/ssrmountainsblog.git ```
 
Go to app's directory:

```  cd ssrmountainsblog ```

Install dependencies:

``` npm i```

And run it!

```  npm run dev```

By default app will run on http://localhost:3000/

----
### License:
The MIT License (MIT)

## Спасибо за просмотр  & Thank you for watching =)
 
