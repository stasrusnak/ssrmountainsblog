const {model, Schema} = require('mongoose')
const userSchema = new Schema({
  login: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    unique: true,
    required: true,
    minLength: 3
  },
})


module.exports = model('users', userSchema)
