const bCrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const User = require('../models/user.model')
const keys = require('../server/keys')

module.exports.login = async (req, resp) => {
  const candidate = await User.findOne({
    login: req.body.login
  })

  if (candidate) {
    const passwordCorrect = bCrypt.compareSync(req.body.password, candidate.password)
    if (passwordCorrect) {
      const token = jwt.sign({
        login: candidate.login,
        userId: candidate._id
      }, keys.JWT, {expiresIn: 60 * 60})

      resp.json({token})

    } else {
      resp.status(401).json({message: 'Пароль не верный'})
    }

  } else {
    resp.status(404).json({message: 'Пользователь не найден'})
  }
}


module.exports.createUser = async (req, resp) => {

  const candidate = await User.findOne({
    login: req.body.login
  })

  if (candidate) {
    resp.status(409).json({message: 'Логин занят'})
  } else {
    const salt = bCrypt.genSaltSync(10)

    const user = new User({
      login: req.body.login,
      password: bCrypt.hashSync(req.body.password, salt)
    })

    await user.save()
    resp.status(201).json(user)
  }
}
