const Comment = require('../models/comment.model')
const Post = require('../models/post.model')

module.exports.create = async (req, res) => {
  try {
    const {name, text, postId} = req.body
    const comment = new Comment({name, text, postId})
    await comment.save()
    const post = await Post.findById(postId)
    post.comments.push(comment._id)
    await post.save()
    res.status(201).json(comment)

  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getAll = async (req, res) => {
  try {
    const comments = await Comment.find().sort({date: -1})
    res.json(comments)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.delete = async (req, res) => {
  async function remove(id) {
    const comment = await Comment.findById(id)
    const post = await Post.findById(comment.postId)
    post.comments.pull(id)
    post.save()
  }
  try {
    req.body.forEach(el => remove(el))
    await Comment.deleteOne({_id: req.body.id})
    res.json({massage: 'Комментарий удален'})
  } catch (e) {
    res.status(500).json(e)
  }
}
