const {Router} = require('express')
const ctr = require('../controllers/comment.controller')
const passport = require('passport')
const router = Router()

// /api/comment
router.post('/', ctr.create)


router.get(
  '/',
  passport.authenticate('jwt', {session: false}),
  ctr.getAll
)

router.post(
  '/delete/',
  passport.authenticate('jwt', {session: false}),
  ctr.delete
)

module.exports = router
