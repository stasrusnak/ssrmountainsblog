import cors from 'cors';
const express = require('express')
const bodyParser = require('body-parser')
const passport = require('passport')
const passportStrategy = require('../middleware/passport.strategy')
const mongoose = require('mongoose')
const AuthRoutes    = require('./../routes/auth.routes')
const PostRoutes    = require('./../routes/post.routes')
const CommentRoutes = require('./../routes/comment.routes')

const app = express()
const keys = require('./keys')


mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false);
mongoose.connect(keys.MONGO_URL, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
})
  .then(() => console.log('mongodb connected!'))
  .catch(error => console.log('mongodb connected error! :' + error))

app.use(cors());
app.use(passport.initialize())
passport.use(passportStrategy)

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use('/api/auth', AuthRoutes)
app.use('/api/post', PostRoutes)
app.use('/api/comment', CommentRoutes)



module.exports = app

