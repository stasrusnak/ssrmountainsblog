module.exports = {
  moduleFileExtensions: [
    "js",
    "json",
    "vue"
  ],
  roots: [
    "tests/unit"
  ],
  watchman: false,
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/$1",
    "^~/(.*)$": "<rootDir>/$1",
    "^~~/(.*)$": "<rootDir>/$1"
  },
  transform: {
    "^.+\\.js$": "babel-jest",
    ".*\\.(vue)$": "vue-jest"
  },
  snapshotSerializers: [
    "jest-serializer-vue"
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    "**/*.{js,vue}",
    "!**/node_modules/**"
  ],
  testURL: "http://localhost/"
};
