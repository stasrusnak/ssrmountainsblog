import Cookie from 'cookie'
import Cookies from 'js-cookie'
import JwtDecode from 'jwt-decode'

export const state = () => ({
  token: null
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  },
  clearToken(state) {
    state.token = null
  }
}

export const actions = {
  async login({commit, dispatch}, formData) {
    try {
      const {token} = await this.$axios.$post('/api/auth/admin/login', formData)
      dispatch('setToken', token)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },
  async createUser({commit, dispatch}, formData) {
    try {

      await this.$axios.$post('/api/auth/admin/create', formData)
      // dispatch('createUser', user)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },
  setToken({commit}, token) {
    this.$axios.setToken(token, 'Bearer')
    commit('setToken', token)
    Cookies.set('jwt-token', token)
  },
  logout({commit}) {
    this.$axios.setToken(false)
    commit('clearToken')
    Cookies.remove('jwt-token')
  },
  autoLogin({dispatch}) {
    const cookiesStr = process.browser
      ? document.cookie
      : this.app.context.req.headers.cookie

    const cookies = Cookie.parse(cookiesStr || '') || {}
    const token = cookies['jwt-token']

    if (IsJWTValid(token)) {
      dispatch('setToken', token)
    } else {
      dispatch('logout')
    }

    function IsJWTValid(token) {
      if (!token) return false
      const jwtData = JwtDecode(token) || {}
      const expires = jwtData.exp || 0
      return (new Date().getTime() / 1000) < expires
    }

  }
}

export const getters = {
  isAuthenticated: state => Boolean(state.token),
  token: state => state.token
}


