export const actions = {
  async create({commit}, data) {
    try {
      return await this.$axios.$post('api/comment', data)
    } catch (e) {
      commit('setError', e, {root: true})
    }
  },
  async delete({commit}, data) {
    try {
      return await this.$axios.$post('api/comment/delete', data)
    } catch (e) {
      commit('setError', e, {root: true})
    }
  },
  async fetchComments({commit}) {
    try {
      return await this.$axios.$get('api/comment')
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },


}
