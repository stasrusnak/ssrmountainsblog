import Cookie from 'cookie'
import Cookies from 'js-cookie'

export const state = () => ({
  color: null
})

export const mutations = {
  setColor(state, token) {
    state.color = token
  },
  clearColor(state) {
    state.color = null
  }
}

export const actions = {

  setColor({commit}, color) {
    commit('setColor', color)
    Cookies.set('cite-color', color)
    this.$colorMode.preference = color
  },
  clearColor({commit}) {
    commit('clearColor')
    Cookies.remove('cite-color')
  },
  autoLoadColor({dispatch}) {
    const cookiesStr = process.browser
      ? document.cookie
      : this.app.context.req.headers.cookie

    const cookies = Cookie.parse(cookiesStr || '') || {}
    const color = cookies['cite-color']
    if (color === undefined) {
      dispatch('clearColor')
    } else {
      dispatch('setColor', color)
    }

  }

}

export const getters = {
  color: state => state.color
}


