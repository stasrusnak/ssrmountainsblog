export const state = () => ({
  posts: null
})

export const mutations = {
  setPosts(state, posts) {
    state.posts = posts
  },
  clearPosts(state) {
    state.posts = null
  }
}


export const actions = {
  async fetchPosts({commit}) {
    try {
      const posts = await this.$axios.$get('/api/post')
      return posts
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },
  async fetchPostById({commit}, id) {
    try {
      return await this.$axios.$get('/api/post/' + id)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },

  async fetchAdminPosts({commit}) {
    try {
      return await this.$axios.$get('/api/post/admin')
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },

  async fetchAdminPostById({commit}, id) {
    try {
      return await this.$axios.$get('/api/post/admin/' + id)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },

  async create({commit}, {title, text, image}) {
    try {
      const fd = new FormData()
      fd.append('title', title)
      fd.append('text', text)
      fd.append('image', image.raw, image.name)

      return await this.$axios.$post('api/post/admin', fd)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },
  async remove({commit}, id) {
    try {
      return await this.$axios.$delete('/api/post/admin/' + id)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },

  async update({commit}, {id, text, title, image}) {
    try {
      const fd = new FormData()
      fd.append('title', title)
      fd.append('text', text)
      fd.append('image', image.raw, image.name)

      return await this.$axios.$put('/api/post/admin/' + id, fd)
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },

  async addView({commit}, {views, _id}) {
    try {

      return await this.$axios.$put('/api/post/view/' + _id, {views})
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  },

  async getAnalytics({commit}) {
    try {
      return await this.$axios.$get('/api/post/admin/get/analytics')
    } catch (e) {
      commit('setError', e, {root: true})
      throw e
    }
  }
}


export const getters = {
  posts: state => state.posts
}
