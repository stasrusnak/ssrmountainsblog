export default function ({store,redirect}) {
  if(!store.getters['baseAuth/isAuthenticated']){
      redirect('/admin/login?msg=login')
  }
}
