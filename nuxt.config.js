export default {
  mode: 'universal',
  target: 'server',
  head: {
    title: process.env.npm_package_name || 'NUXT BLOG',
    meta: [
      {charset: "utf-8"},
      {name: "viewport", content: "width=device-width, initial-scale=1"},
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [{rel: "icon", type: "image/x-icon", href: "/favicon.ico"}]
  },
  css: [
    'element-ui/lib/theme-chalk/index.css',
    './theme/index.scss'

  ],
  loading: {
    color: '#409EFF',
    height: '5px'
  },

  plugins: [
    '~/plugins/global',
    '~/plugins/axios',
  ],

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/pwa',
  ],
  modules: [
    '@nuxt/http',
    [
      "@nuxtjs/axios",
      {
        baseURL: process.env.BASE_URL || 'http://localhost:3000'
      }
    ],
    '@nuxtjs/color-mode'
  ],

  pwa: {
    manifest: {
      name: 'Nuxt mountains blog',
      short_name: 'Mountains blog',
      theme_color: "#2196F3",
    }
  },

  axios: {

  },

  serverMiddleware: [
    '~/api/index.js',
  ],
  build: {}
};
